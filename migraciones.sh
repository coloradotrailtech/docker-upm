 docker-compose exec php-upm composer install

 docker-compose exec php-upm php artisan key:generate
 
 docker-compose exec php-upm chmod -R 777 /code/storage
 
 docker-compose exec php-upm chmod -R 777 /code/bootstrap
 
 docker-compose exec php-upm chmod -R 777 /code/public 

 docker-compose exec php-upm php artisan migrate:refresh

 docker-compose exec php-upm php artisan db:seed