  #!/bin/bash

 mkdir app
 
 git clone https://hachoka@bitbucket.org/coloradotrailtech/upm.git

 cp .env.laravel ./upm/src/.env

 mv upm ./app/upm

 mkdir mysql  
 
 mkdir mysql/data  
 
 mkdir mysql/backup
  
 docker-compose up -d